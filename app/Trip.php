<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


class Trip extends Model
{
    protected $table = 'trips';

    public function agent(){
        return $this->belongsTo('App\User','user_id','id');
    }
    public static function searchForTrips($tripType,$countryFrom,$countryTo,$dateFrom,$dateTo){
        $result = self::where('trip_type',$tripType)
        ->where('from_country',$countryFrom)
        ->where('to_country',$countryTo)
        ->where('trip_start_date','<=' ,$dateFrom)
        ->where('trip_end_date','>=' ,$dateTo)
        ->get();

        return $result;
    }

}