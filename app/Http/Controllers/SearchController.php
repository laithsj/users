<?php

namespace App\Http\Controllers;
use App;
use Illuminate\Http\Request;
use Redirect;
use App\Trip;

class SearchController extends Controller
{
    public function search(Request $request){
        // check if trip is global or enternal
        $tripType = '1'; // hay lazem etmarge7a mn ellfront end , 3ashan el search ykon based 3ala external or internal
        if($tripType == '1'){
            // get destination from and destination to
            $countryFrom = $request->fromDest;
            $countryTo = $request->toDest;


            // for test
            $countryFrom = 'af';
            $countryTo = 'al';

            // get date from and date to || convert date to laravel formate
            $dateFrom = date('Y-m-d 00:00:00', strtotime(str_replace("/", "-", $request->dateFrom)));
            $dateTo   = date('Y-m-d 23:59:59', strtotime(str_replace("/", "-", $request->dateTo)));

            // for test
            $dateFrom = '2019-11-27 18:21:12';
            $dateTo = '2019-11-30 18:21:12';

            // earch for trips 
            $tripsResult    =   Trip::searchForTrips($tripType,$countryFrom,$countryTo,$dateFrom,$dateTo);
            return view('search',['trips' => $tripsResult]);
        }

        return redirect(action('SearchController@viewSearch'));
    }
     
    public function viewSearch(Request $request){
        return view('search');
    }

}