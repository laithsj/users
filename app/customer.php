<?php

namespace App;

use Illuminate\Foundation\Auth\customer as Authenticatable;
use Illuminate\Notifications\Notifiable;

class customer extends Authenticatable
{
   

    public static function checkIfAgentExist($email){
        return self::where('email',$email)->get();
    }

    public function addNewCustomer($request,$password){
        $this->name     =   $request->name;
        $this->email    =   $request->email;
        $this->password =   $password;
        $this->save();

        return $this;
    }


}
