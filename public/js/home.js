$(document).ready(function(){
    $(".btn-secondary").click(function(){
        $(".btn-group").fadeOut(0);
      });
      $(".btn-secondary").click(function(){
        $(".globalcard").fadeIn(700);
      });  
      $(".btn-light").click(function(){
        $(".btn-group").fadeOut(0);
      });
      $(".btn-light").click(function(){
        $(".localcard").fadeIn(700);
      });  
      $( ".datepicker" ).datepicker();

      $('#startdate').datepicker({
        next: '#enddate' // The date in '#startdate' must be before or equal to the date in '#enddate'
      });
      $('#enddate').datepicker({
        previous: '#startdate' // The date in '#enddate' must be after or equal to the date in '#startdate'
      });
  /* this for accsisibilty to set focus on the first input */
  $('.trip-search-gloabel').click(function(){ // this class was added in the button to set event click on them
    var tripId = '#'+$(this).attr('data-id'); 
    /*
      data-id is an attribute that I defined it in the button to reffer to the box  that have this ID 

      I added this attribute to reffer to the box that have id === data-id
      and then find the first input element and focus in it 	

      Example 
        <button data-id="globleTripSearchPart"></button>
        <div id="globleTripSearchPart">.. THE INPUTS HTML ..</div>

        The selector that I make it in line 18  will look like this 
        var tripId = '#' + 'globleTripSearchPart';

        The finall selector it will look like this 
        $(tripId) = $('#globleTripSearchPart') 
     */
    $(tripId).find('input').first().focus();
  });

  // This for moadel when the user open it 

  $('.login-action').click(function(){
      $('.login-user').focus(); // to get the login-user link and focus it 
  });
  $('.log_in').click(function(){
    $('.modal-title').focus(); // to get the login-user link and focus it 
});
  });